/**
 * 
 */
package com.nespresso.recruitment.gossip;

/**
 * @author pankaj.kumar.gautam
 * 
 */
public class Person {

	private String personName;
	private String personTitle;
	private Person listenFrom;
	private String message;

	public Person(String title, String name) {
		this.personName = name;
		this.personTitle = title;
	}

	public Person() {
		// TODO Auto-generated constructor stub
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getPersonTitle() {
		return personTitle;
	}

	public void setPersonTitle(String personTitle) {
		this.personTitle = personTitle;
	}

	public Person getListenFrom() {
		return listenFrom;
	}

	public void setListenFrom(Person listenFrom) {
		this.listenFrom = listenFrom;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
