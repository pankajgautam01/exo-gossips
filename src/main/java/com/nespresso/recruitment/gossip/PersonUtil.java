/**
 * 
 */
package com.nespresso.recruitment.gossip;

import java.util.HashMap;
import java.util.Map;

/**
 * @author pankaj.kumar.gautam
 * 
 */
public class PersonUtil {

	public static Map<String, Person> populatePerson(String[] listners) {

		Map<String, Person> personMap = new HashMap<String, Person>();

		for (String listner : listners) {
			String title = listner.split(" ")[0];
			String name = listner.split(" ")[1];
			personMap.put(name, new Person(title, name));
		}

		return personMap;
	}
}
