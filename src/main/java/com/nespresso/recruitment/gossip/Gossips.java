package com.nespresso.recruitment.gossip;

import java.util.HashMap;
import java.util.Map;

/**
 * @author pankaj.kumar.gautam
 * 
 */
public class Gossips {

	private Map<String, Person> personMap = new HashMap<String, Person>();
	private Person personFrom;
	private String message;

	public Gossips(String... listners) {
		this.personMap = PersonUtil.populatePerson(listners);

	}

	public Gossips from(String fromString) {
		this.personFrom = personMap.get(fromString);
		// this.communicate = new Communicate(personFrom, this);
		return this;
	}

	public Gossips to(String toString) {
		Person personTo = personMap.get(toString);
		if (null != this.message) {
			personTo.setMessage(this.message);
		} else if (null != this.personFrom) {
			personTo.setListenFrom(this.personFrom);
			this.personFrom = null;
		}
		return this;
	}

	public Gossips say(String message) {
		this.message = message;
		return this;
	}

	public String ask(String personString) {
		Person person = personMap.get(personString);
		return person.getMessage();
	}

	public void spread() {
		Person personHasMessage = new Person();
		String message = "";
		for (Person person : personMap.values()) {
			if (null != person.getMessage() && !person.getMessage().isEmpty()) {
				personHasMessage = person;
				message = new String(person.getMessage());
				person.setMessage("");
			} else if (null != personHasMessage
					&& null != person.getListenFrom()
					&& person.getListenFrom().getPersonName()
							.equals(personHasMessage.getPersonName())) {
				person.setMessage(message);
			}

		}
	}
}
